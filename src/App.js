import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { Provider } from 'react-redux';
import store from './redux/store'
import Board from './Components/Board/Board';
import Boards from './Components/Boards/Boards';
import CardDetails from './Components/CardDetails/CardDetails';




function App() {
  return (
    <div className="App">
      <div className='header'>Trello</div>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" render={() => {
              return (<Redirect to="/boards" />)
            }} />

            <Route exact path="/boards" component={Boards} />

            <Route path="/boards/:boardId" component={Board} />
              
            <Route path="/card/:cardId" component={CardDetails}/>

              <Route path="*" element={<h1>PAGE NOT FOUND</h1>} />


          </Switch>

        </Router>
      </Provider>

    </div>
  );
}

export default App;
