import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Cards from '../Cards/Cards'
import style from './Board.module.css'
import Star from '../star.svg'
import Bin from './bin.svg'


import { getLists, createList, deleteList } from '../../redux/ListRedux/listActions'

class Board extends Component {  
  constructor(props) {
    super(props);       
    this.state = {
      boardDetails: this.props.boards.filter((x) => this.props.match.params.boardId === x.id)[0],
      lists: { ...this.props.lists },
      boardId: this.props.match.params.boardId,
      addCard: false,
    }

  }

  componentDidMount() {
    this.props.getLists(this.state.boardId)
  }
  componentDidUpdate(prevProps) {
    if (prevProps.lists !== this.props.lists) {

      this.setState({ ...this.state, lists: { ...this.props.lists } })
    }
  }



  render() {

    let createList = (e) => {
      e.preventDefault();
      this.props.createList(e.target.listName.value, this.state.boardId)

    }



    let { allLists, error, loading } = this.state.lists
    if (loading) {
      return <div>Loading...</div>
    } else if (error) {
      return <div>Loading Failed</div>
    } else {
      return <div className={style.board}
        style={{
          backgroundImage: `url(${{ ...this.state.boardDetails.prefs }.backgroundImage})`,
          backgroundColor: `${{ ...this.state.boardDetails.prefs }.backgroundColor}`
        }}
      >
       
        <div className={style.header} >
          <div>  <img src={Star} className={style.star} alt="" /></div>
          <h1>{this.state.boardDetails.name}</h1>
        </div>
        <form onSubmit={(e) => createList(e)}>
          <div className={style.addlist}>

            <div><input className={style.input} name="listName" placeholder='Add new list...'></input></div>
            <div><Button type="submit">Add</Button></div>


          </div>
        </form>

        <div className={style.lists}>{
          allLists.map(list => {
            return <div key={list.id}>
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title className={style.cardTitle}>
                    <h3> {list.name}</h3>
                    <div> <img src={Bin} 
                     onClick={() => this.props.deleteList(list.id)}
                    className={style.bin} alt="" /></div>
                   
                  </Card.Title>



                  <Cards listId={list.id} />





                </Card.Body>

              </Card>
            </div>

          })
        }
        </div>
       

      </div>
    }
  }
}


let mapStateToProps = state => {
  return {
    boards: state.boards.allBoards,
    lists: state.lists
  }
}
let mapDispatachToProps = dispatch => {
  return {
    getLists: (id) => dispatch(getLists(id)),
    createList: (name, boardId) => dispatch(createList(name, boardId)),
    deleteList: (listId) => dispatch(deleteList(listId))
  }
}

export default connect(mapStateToProps, mapDispatachToProps)(Board);