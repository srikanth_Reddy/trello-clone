import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  getCheckList, createCheckList, updateCheckItem,
  deleteCheckList, createCheckItem, deleteCheckItem
} from '../../redux/CheckList-ItemRedux/checkListActions'
import style from './CardDetails.module.css'
import Bin from './bin.svg'

class CardDetails extends Component {  
  constructor(props) {
    super(props);
    this.state = {}
  }
  

  componentDidMount() {
    this.props.getCheckList(this.props.match.params.cardId)
  }


  render() {


    let createChecklist = (e) => {
      e.preventDefault();

      this.props.createCheckList(e.target.listName.value, this.props.match.params.cardId)
    }

    let deleteChecklist = (cardId, listId) => {
      this.props.deleteCheckList(cardId, listId)
    }

    let addCheckItem = (e, listId) => {
      e.preventDefault()
      this.props.createCheckItem(e.target.itemName.value, listId)
    }

    let delCheckItem = (listId, itemId) => {
      this.props.deleteCheckItem(listId, itemId)
    }

    let updateCheckITEM = (idCard, idChecklist, idCheckItem, state) => {
      this.props.updateCheckItem(idCard, idChecklist, idCheckItem, state)
    }

    let { loading, Checklist, error } = this.props.checkList

    if (loading) {
      return <div>Loading...</div>
    } else if (error) {
      return <div>Loading Failed...</div>
    } else {

      
      let a=((Object.values(this.props.allCards)).flat(1))?.filter((card)=>card.id === this.props.match.params.cardId)
      

      return (<div className={style.list}>

        <div>
          <h2>{a[0].name}</h2>
          <form
            onSubmit={(e) => { createChecklist(e) }}
          >
            <input placeholder='+Add new check list' name="listName"></input><button>+Add</button>
          </form>
        </div><br /><br />

        {
          Checklist.map((list) => {
            return <div key={list.id}>
              <div className={style.listhead}>
                <div><h3> {list.name}</h3></div>
                <div><img src={Bin} alt=""
                  className={style.listBtn}
                  onClick={() => deleteChecklist(this.props.match.params.cardId, list.id)}
                ></img></div>
              </div>
              <div className={style.checkItems}>{list.checkItems.map(item => {
                return <div key={item.id}>
                  <div className={style.checkAlign}> <div> {item.state === "incomplete" && <><input type="checkbox"

                    onChange={() => updateCheckITEM(this.props.match.params.cardId, list.id, item.id, "complete")}
                  /><span>{item.name} </span></>}
                    {item.state === "complete" && <><input type="checkbox"
                      onChange={() => updateCheckITEM(this.props.match.params.cardId, list.id, item.id, "incomplete")}
                      checked /><span className={style.checkItem}>{item.name} </span></>} </div>
                    <button onClick={() => delCheckItem(list.id, item.id)}>x</button>  </div>
                </div>
              })}
                <br />

                <div>
                  <form onSubmit={(e) => addCheckItem(e, list.id)}>
                    <input placeholder='Add Check Item...' name="itemName"></input><button type="submit">+Add</button>
                  </form>
                </div>

              </div>
              <br />
            </div>
          })
        }

      </div>)
    }
  }
}


let mapStateToProps = state => {
  return {
    checkList: state.checkList,
    allCards: state.cards.allCards
  }

}
let mapDispatachToProps = dispatch => {
  return {
    getCheckList: (cardId) => dispatch(getCheckList(cardId)),
    createCheckList: (listName, cardId) => dispatch(createCheckList(listName, cardId)),
    deleteCheckList: (cardId, listId) => dispatch(deleteCheckList(cardId, listId)),
    createCheckItem: (itemName, listId) => dispatch(createCheckItem(itemName, listId)),
    deleteCheckItem: (listId, itemId) => dispatch(deleteCheckItem(listId, itemId)),
    updateCheckItem: (idCard, idChecklist, idCheckItem, state) => dispatch(updateCheckItem(idCard, idChecklist, idCheckItem, state))
  }
}

export default connect(mapStateToProps, mapDispatachToProps)(CardDetails);