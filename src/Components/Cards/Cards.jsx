import React, { Component } from 'react'
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import style from './Cards.module.css'


import { getCards, createCard, deleteCard } from '../../redux/CardRedux/cardActions'

class Cards extends Component {
  constructor(props) {
    super(props); 
    this.state = {

      addCard:{},
      cardId: "",
      cardName: ""
    }

  }

  componentDidMount() {
    this.props.getCards(this.props.listId)
  }



  render() {

    let CreateCard = (e, listId) => {
      e.preventDefault()
      this.setState({...this.state,addCard:{[this.props.listId]:false}})
      this.props.createCard(e.target.cardName.value, listId)
    }

    let delCard = (cardId, listId) => {
      this.props.deleteCard(cardId, listId)
    }

    let handlePopUp = (cardId, name) => {
      this.setState({ ...this.state, popUp: true, cardId: cardId, cardName: name })
    }

    let { loading, allCards, error } = this.props.cards

    if (loading) {
      return <div>Loading...</div>
    } else if (error) {
      return <div>Loading Failed...</div>
    } else {

      return (<>
        <div>{this.state.listId}</div>
        <Card style={{ width: '15rem' }}>
          <ListGroup variant="flush">
            {allCards[this.props.listId]?.map((card) => {

              return <ListGroup.Item key={card.id}>
                <div className={style.card}>
          
                  <Link to={`/card/${card.id}`} className={style.link}> <div className={style.cardName}
                    onClick={() => handlePopUp(card.id, card.name)}>
                    {card.name} </div></Link>
                    
                  <div><button onClick={() => delCard(card.id, this.props.listId)}>X</button></div>
                </div>
              </ListGroup.Item>
            })}

          </ListGroup>
          {!this.state.addCard[this.props.listId] && <Button variant="primary"
          onClick={()=>this.setState({...this.state,addCard:{[this.props.listId]:true}})}
          >+Add Card</Button>}

          {(this.state.addCard[this.props.listId])
            && <form onSubmit={(e) => CreateCard(e, this.props.listId)}>
              <input className={style.input} style={{ width: '12rem' }} name="cardName" placeholder="Enter card name..." />
              <Button type="submit" className={style.btn}>Add</Button>
              <Button className={style.btn} 
              onClick={()=> this.setState({...this.state,addCard:{[this.props.listId]:false}})}
              variant='danger'>X</Button>
            </form>}
        </Card>

      </>)
    }
  }
}

let mapStateToProps = state => {
  return {
    cards: state.cards
  }

}
let mapDispatachToProps = dispatch => {
  return {
    getCards: (listId) => dispatch(getCards(listId)),
    createCard: (cardName, listId) => dispatch(createCard(cardName, listId)),
    deleteCard: (cardId, listId) => dispatch(deleteCard(cardId, listId))
  }
}

export default connect(mapStateToProps, mapDispatachToProps)(Cards);