import React, { Component } from 'react'
import style from './Boards.module.css'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import {
  getBoards,
  createBoard
} from '../../redux/BoardRedux/boardActions'




class Boards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      handleBoard: 0, boards: { ...this.props.boards },
    }

  }
  componentDidMount() {
    this.props.getBoards()
  }
  componentDidUpdate(prevProps) {
    if (prevProps.boards !== this.props.boards) {
      this.setState({ ...this.state, boards: { ...this.props.boards } })
    }
  }

  render() {

    let handleCreateBoard = (e) => {
      e.preventDefault();
      this.setState({ ...this.state, handleBoard: 0 })
      this.props.createBoard(e.target.boardName.value)
    }


    // console.log(this.state.boards)
    let { allBoards, error, loading } = this.state.boards

    let createBoardStyle = { margin: "1rem", width: "20%", height: "20vh", border: "1px solid rgb(200,200,200)", display: "flex", flexDirection: "column", justifyContent: "center", backgroundColor: "rgb(235,235,235)", borderRadius: "10px" }
    let board = { display: "flex", flexWrap: "wrap" }
    return (<>
      <h2>Boards</h2>

      <div className='boards' style={board}>
        <div className='createBoard' style={createBoardStyle} >

          {!this.state.handleBoard
            ? <div onClick={() => this.setState({ ...this.state, handleBoard: 1 })}>Create Board</div>
            : <div >
              <div>
                <form
                  onSubmit={(e) => handleCreateBoard(e)}
                >
                  <input name="boardName" className={style.input} placeholder="Enter board name..." />
                  <button type="submit" className={style.btn} >Create Board</button>
                  <button className={style.btn} onClick={() => this.setState({ ...this.state, handleBoard: 0 })}>Cancle</button>
                </form>
              </div>
              <br />


            </div>
          }
        </div>

        {loading && <div>Loading...</div>}
        {error && <div>Loading Failed...</div>}
        {(!loading && !error) && <div className={style.boards}>{

          allBoards.map((board) => {
            return <Link key={board.id} className={style.Link} to={`/boards/${board.id}`}>
              <div
                className={style.board}
                onClick={() => { }}
                style={{
                  backgroundImage: `url(${{ ...board.prefs }.backgroundImage})`,
                  backgroundColor: `${{ ...board.prefs }.backgroundColor}`
                }}>

                {board.name}
              </div>
            </Link>
          })
        }</div>}
      </div>
    </>)
  }
}

let mapStateToProps = state => {
  return { boards: state.boards }
}

let mapDispatachToProps = dispatch => {
  return {
    getBoards: () => dispatch(getBoards()),
    createBoard: (boardName) => dispatch(createBoard(boardName))

  }
}

export default connect(mapStateToProps, mapDispatachToProps)(Boards);

