import { combineReducers } from "redux";
import boardsReducer from "./BoardRedux/boardReducer";
import listsReducer from "./ListRedux/listReducer";
import cardsReducer from "./CardRedux/cardReducer";
import checkListReducer from './CheckList-ItemRedux/checkListReducer'

const rootReducer = combineReducers({
    boards:boardsReducer,
    lists:listsReducer,
    cards:cardsReducer,
    checkList:checkListReducer
})


export default rootReducer;