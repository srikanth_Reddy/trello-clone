let initialState = {
    loading: false, allCards: {}, error: null
}
let cardsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CARDS_REQ":
            return { ...state, loading: true }
        case "GET_CARDS_SUC":
            return { ...state, loading: false, allCards: { ...state.allCards, [action.listId]: action.payload } }
        case "GET_CARDS_FAIL":
            return { ...state, loading: false, error: action.payload }
        case "CREATE_CARD":
            return { ...state, allCards: { ...state.allCards, [action.listId]: [...state.allCards[action.listId], { ...action.payload }] } }
        case "DEL_CARD":
            return {
                ...state,
                allCards: {
                    ...state.allCards,
                    [action.listId]: state.allCards[action.listId].filter((card) => card.id !== action.cardId)
                }
            }
        // return {...state, allCards:state.allCards.filter(x=> x.id !== action.payload.id)}
        default:
            return state
    }
}

export default cardsReducer;