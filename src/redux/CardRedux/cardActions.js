import axios from "axios"


let apiKey = "22382408d229176979776f23a95a3fd7"
let apiToken = "fe9096979332a0b270ce10bbb7ae19fc226fbe176a33b98ed35afd739d244d87"


export const getCards= (listId) => async dispatch =>{
    dispatch( {type:"GET_CARDS_REQ"})
    await axios.get(`https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data
        dispatch({
            type:"GET_CARDS_SUC",
            payload:data,
            listId:listId
        })
    })
    .catch((error)=> dispatch({
        type:"GET_CARDS_FAIL",
        payload:error
    }))

}

 
export const createCard= (cardName, listId) => dispatch =>{
    axios.post(`https://api.trello.com/1/cards?idList=${listId}&key=${apiKey}&token=${apiToken}`,{name:cardName})
    .then(res=>{
        const data=res.data
        dispatch({
            type:"CREATE_CARD",
            payload:data,
            listId:listId
        })
    })
}


export const deleteCard = (cardId, listId) => dispatch =>{ 
   axios.delete(`https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`)
   .then(res=>{
    const data=res.data
    dispatch({
        type:"DEL_CARD",
        payload:data,
        listId:listId,
        cardId:cardId
    })
   })
}
