import axios from "axios"


let apiKey = "22382408d229176979776f23a95a3fd7"
let apiToken = "fe9096979332a0b270ce10bbb7ae19fc226fbe176a33b98ed35afd739d244d87"

 
export const getLists= (id) => dispatch =>{ 
    dispatch( {type:"GET_LISTS_REQ"})
    axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data
        dispatch({
            type:"GET_LISTS_SUC",
            payload:data
        })
    })
    .catch((error)=> dispatch({
        type:"GET_LISTS_FAIL",
        payload:error
    }))

}

 
export const createList= (name, boardId) => dispatch =>{
    axios.post(`https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data
        dispatch({
            type:"CREATE_LIST",
            payload:data
        })
    })
}


export const deleteList = (listId) => dispatch =>{ 
   axios.put(`https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`,{closed:true})
   .then(res=>{
    const data=res.data
    dispatch({
        type:"DEL_LIST",
        payload:data
    })
   })
}

