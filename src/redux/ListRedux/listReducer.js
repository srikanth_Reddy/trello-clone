let initialState = { 
    loading:false, allLists:[], error:null
}
let listsReducer = (state=initialState, action) =>{
    switch(action.type){
        case "GET_LISTS_REQ":
            return {loading:true }
        case "GET_LISTS_SUC":
            return {loading:false, allLists:action.payload } 
        case "GET_LISTS_FAIL":
            return {loading:false, error:action.payload }
        case "CREATE_LIST":
            return {...state, allLists:[...state.allLists, {...action.payload}]}
        case "DEL_LIST":
            return {...state, allLists:state.allLists.filter(x=> x.id !== action.payload.id)}
        default:
            return state
    }
}

export default listsReducer;