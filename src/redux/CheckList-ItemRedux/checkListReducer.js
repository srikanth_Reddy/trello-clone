let initialState = {
    loading: false, Checklist:[], error: null
}
let cardsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CHECKLIST_REQ":
            return { ...state, loading: true }
        case "GET_CHECKLIST_SUC":
            return { ...state, loading: false, Checklist:action.payload }
        case "GET_CHECKLIST_FAIL":
            return { ...state, loading: false, error: action.payload }
        case "CREATE_CHECKLIST":
            return {...state,Checklist:[...state.Checklist, {...action.payload}]}
        case "DEL_CHECKLIST":
            return {...state, Checklist:state.Checklist.filter((x)=> x.id !== action.listId)}
        case "CREATE_CHECKITEM":
            let {idChecklist} = action.payload;
            let Checklist = state.Checklist.reduce((acc,cur)=>{
                if(cur.id === idChecklist){
                    let checkItems= [...cur.checkItems, {...action.payload}]
                    acc.push({...cur, checkItems:[...checkItems]})
                    return acc
                }else{
                    acc.push(cur)
                    return acc
                }
            },[])

            return {...state, Checklist:Checklist}

        case "DEL_CHECKITEM":
            let Checklists = state.Checklist.reduce((acc,cur)=>{  
                if(cur.id === action.listId){
                    acc.push({...cur, checkItems:[...cur.checkItems.filter((x)=> x.id!==action.itemId)]})
                }else{
                    acc.push(cur)
                    return acc
                }
                return acc;
            },[])
            return {...state, Checklist:Checklists}

        case "UPDATE_CHECKITEM":
            let checkList = state.Checklist.reduce((acc,list)=>{
                if(list.id === action.listId){

                    let checkItems = list.checkItems.map((item)=>{
                        if(item.id === action.itemId){
                            return {...item, state:action.state}
                        }else{
                            return item
                        }
                    })

                    acc.push({...list, checkItems:checkItems})
                     
                }else{
                    acc.push(list)
                }
                return acc;
            },[])
            return {...state, Checklist:checkList}

        default:
            return state
    }
}

export default cardsReducer;