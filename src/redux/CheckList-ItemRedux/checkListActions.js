import axios from "axios"


let apiKey = "22382408d229176979776f23a95a3fd7"
let apiToken = "fe9096979332a0b270ce10bbb7ae19fc226fbe176a33b98ed35afd739d244d87"


export const getCheckList= (cardId) => async dispatch =>{
    dispatch( {type:"GET_CHECKLIST_REQ"})
    await axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data
        dispatch({
            type:"GET_CHECKLIST_SUC",
            payload:data,
            listId:cardId
        })
    })
    .catch((error)=> dispatch({
        type:"GET_CHECKLIST_FAIL",
        payload:error
    }))

}

 
export const createCheckList= (listName, cardId) => dispatch =>{
    axios.post(`https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`,{name:listName})
    .then(res=>{
        const data=res.data 
        dispatch({
            type:"CREATE_CHECKLIST",
            payload:data,
            
        })
    })
}


export const deleteCheckList = (cardId,listId) => dispatch =>{ 
   axios.delete(`https://api.trello.com/1/cards/${cardId}/checklists/${listId}?key=${apiKey}&token=${apiToken}`)
   .then(res=>{
    const data=res.data
    dispatch({
        type:"DEL_CHECKLIST",
        payload:data,
        listId:listId,
        cardId:cardId
    })
   })
}


// checkItems

export const createCheckItem= (itemName, listId) => dispatch =>{
    axios.post(`https://api.trello.com/1/checklists/${listId}/checkItems?name=${itemName}&key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data 
        dispatch({
            type:"CREATE_CHECKITEM",
            payload:data,  
        })
    })
}


export const deleteCheckItem = (listId, itemId) => dispatch =>{ 
   axios.delete(`https://api.trello.com/1/checklists/${listId}/checkItems/${itemId}?key=${apiKey}&token=${apiToken}`)
   .then(res=>{
    const data=res.data
    dispatch({
        type:"DEL_CHECKITEM",
        payload:data,
        listId:listId,
        itemId:itemId
    })
   })
}


export const updateCheckItem = (idCard, idChecklist, idCheckItem, state) => dispatch=>{
    axios.put(`https://api.trello.com/1/cards/${idCard}/checklist/${idChecklist}/checkItem/${idCheckItem}?key=${apiKey}&token=${apiToken}`, {state:state})
    .then((res)=>{
        const data= res.data
        dispatch({
            type:"UPDATE_CHECKITEM",
            payload:data,
            listId:idChecklist,
            itemId:idCheckItem,
            state:state
        })
    })
}


