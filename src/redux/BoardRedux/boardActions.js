import axios from "axios"


let apiKey = "22382408d229176979776f23a95a3fd7"
let apiToken = "fe9096979332a0b270ce10bbb7ae19fc226fbe176a33b98ed35afd739d244d87"

 
export const getBoards= () => dispatch =>{ 
    dispatch( {type:"GET_BOARDS_REQ"})

    axios.get(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data
        dispatch({
            type:"GET_BOARDS_SUC",
            payload:data
        })
    })
    .catch((error)=> dispatch({
        type:"GET_BOARDS_FAIL",
        payload:error
    }))

}

 
export const createBoard= (name) => dispatch =>{
    axios.post(`https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${apiToken}`)
    .then(res=>{
        const data=res.data
        dispatch({
            type:"CREATE_BOARD",
            payload:data
        })
    })
}

