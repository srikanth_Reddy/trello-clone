

let initialState = { 
    loading:false, allBoards:[], error:null
}

let boardsReducer = (state=initialState, action) =>{
    switch(action.type){
        case "GET_BOARDS_REQ":
            return {loading:true }
        case "GET_BOARDS_SUC":
            return {loading:false, allBoards:action.payload } 
        case "GET_BOARDS_FAIL":
            return {loading:false, error:action.payload }
        case "CREATE_BOARD":
            return {...state, allBoards:[...state.allBoards, {...action.payload}]}
        default:
            return state
    }
}

export default boardsReducer;